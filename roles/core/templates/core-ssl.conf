<VirtualHost *:443>
  ServerName {{ domain_core }}
  ServerAdmin {{ mail }}
  Protocols h2 http/1.1
  DocumentRoot /var/www/{{ domain_core }}/public/
  DirectoryIndex index.html index.htm index.php
  <Directory /var/www/{{ domain_core }}/public/>
    Options -Indexes +FollowSymLinks -MultiViews
    AllowOverride All
    Require all granted

    <IfModule mod_php7.c>
      php_value upload_max_filesize 10240K
      php_value max_execution_time 240
      php_value post_max_size 10240K
      php_value opcache.interned_strings_buffer 8
      php_value opcache.max_accelerated_files 10000
      php_value opcache.memory_consumption 128
      php_value opcache.save_comments 1
      php_value opcache.revalidate_freq 1
    </IfModule>
  </Directory>
  <DirectoryMatch ".*/\.(git|svn)/.*">
    Require all denied
  </DirectoryMatch>

  LogLevel warn
  ErrorLog ${APACHE_LOG_DIR}/error.log
  CustomLog ${APACHE_LOG_DIR}/access.log combined

  # This could be an alternative for Apache 2.4.34 and later.
  # https://bz.apache.org/bugzilla/show_bug.cgi?id=62445
  #
  # SSLEngine on
  # <IfFile /etc/letsencrypt/live/{{ domain_core }}/fullchain.pem>
  #   SSLCertificateFile /etc/letsencrypt/live/{{ domain_core }}/fullchain.pem
  # </IfFile>
  # <IfFile /etc/letsencrypt/live/{{ domain_core }}/privkey.pem>
  #   SSLCertificateKeyFile /etc/letsencrypt/live/{{ domain_core }}/privkey.pem
  # </IfFile>
  # <IfFile /etc/letsencrypt/options-ssl-apache.conf>
  #   Include /etc/letsencrypt/options-ssl-apache.conf
  # </IfFile>

  SSLEngine on
  SSLCertificateFile /etc/letsencrypt/live/{{ domain_core }}/fullchain.pem
  SSLCertificateKeyFile /etc/letsencrypt/live/{{ domain_core }}/privkey.pem
  Include /etc/letsencrypt/options-ssl-apache.conf

  LogFormat "%v:%p 0.0.0.0 %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" vhost_combined
  LogFormat "0.0.0.0 %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" combined
  LogFormat "0.0.0.0 %l %u %t \"%r\" %>s %O" common
  LogFormat "%{Referer}i -> %U" referer
  LogFormat "%{User-agent}i" agent

  ErrorLogFormat "[%t] [%l] [pid %P] %F: %E: [client 0.0.0.0] %M"

  Header always set Strict-Transport-Security "max-age=15768000; includeSubDomains"
  Header always append X-Frame-Options "deny"
  Header always set X-Content-Type-Options "nosniff"
  Header always set X-XSS-Protection "1; mode=block"
  Header always set Referrer-Policy "no-referrer"
  Header always set Content-Security-Policy "default-src 'self'"

  SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1
</VirtualHost>
