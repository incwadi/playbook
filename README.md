# incwadi Playbook

The package configures your server to run incwadi. Only things that are actually needed are covered by this playbook.

## Requirements

- Ubuntu
- SSH access
- Two Domains or subdomains
- TLS Certificate for all vHosts

Also, you should be familiar with Ansible, if not you will find a Get Started Guide on [their website](https://www.ansible.com/resources/get-started).

## Updates

The incwadi apps will be updated with this config automatically, but the packages of the OS must be updated by yourself.

## Backup

It should be enough to backup only files where custom changes are made. In most of the cases thats the database on the server and the `vars.yml` and `hosts` files from this playbook.

The apps itself can be reinstalled with this playbook.

## Getting Started

Create the vars file `vars.yml`.

```shell
touch vars.yml
```

Add the following content and fit it to your needs.

```yaml
---
app_secret: SECRET
db_password: PASS
db_user: incwadi
db_name: incwadi
db_host: 127.0.0.1
db_port: 3306
domain_core: DOMAIN
domain_inventory: DOMAIN
mail: MAIL
jwt_passphrase: PASS
currency: EUR
brand_color: "#e1661e"
locale: de
mysql_root_password: PASS
stable_release: 1
```

You should encrypt the `vars.yml` for security reasons.

```shell
ansible-vault encrypt vars.yml
```

You can edit the file with the corresponding command.

```shell
ansible-vault edit vars.yml
```

Create a `hosts` file.

```shell
touch hosts
```

Add the following content to it and change the IP address(es) to the one of your server(s).

```apache
[core]
192.168.0.1

[core:vars]
ansible_python_interpreter=/usr/bin/python3

[inventory]
192.168.0.2

[inventory:vars]
ansible_python_interpreter=/usr/bin/python3
```

### Deploy the Config

Before running this config, update the sources and packages on the machine.

```shell
sudo apt-get update && sudo apt-get dist-upgrade
```

Run the playbook with the following command.

```shell
ansible-playbook server.yml
```

Configure your MySQL Server with the following command.

```shell
mysql_secure_installation
```

Recommended answers:

- VALIDATE PASSWORD PLUGIN no
- Change the password for root? yes
- Remove anonymous users? yes
- Disallow root login remotely? yes
- Remove test database and access to it? yes
- Reload privilege tables now? yes

Now you can (re-)install your certificates. If you wanna use Let's Encrypt all you need is already installed. Just run the following command and get the certificates.

```shell
certbot certonly --apache
```

Run the playbook again with the following command, so the vHosts can be activated.

```shell
ansible-playbook server.yml
```

Reboot your system.

### First Time Install

If you are installing the app for the first time or are reinstalling the server run the following command to initially setup the apps.

```shell
/root/setup-core
/root/setup-inventory
```

For creating branches and users have a look into the documentation for incwadi/core.
